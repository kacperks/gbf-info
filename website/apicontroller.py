import requests

url = "http://localhost:5000"

def add_building(name, location, elevator, cctv, abandoned, yearbuilt):
    try:
        #make a request
        r = requests.post(url + '/add_building', json={'name': name, 'location': location, 'elevator': elevator, 'cctv': cctv, 'abandoned': abandoned, 'yearbuilt': yearbuilt})
        #return the response
        return r.json()
    except Exception as e:
        return {'message': str(e)}

def get_lastobjs():
    try:
        #make a request
        r = requests.get(url + '/lastobjs')
        #return the response
        return r.json()
    except Exception as e:
        return {'message': str(e)}

