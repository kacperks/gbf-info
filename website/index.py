#Sovnat GBF - 
#Created by CubeSoftware
#-------------------------------
#Copyright (c) 2021-2022 CubeSoftware
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the #Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the #Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A #PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION #OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


from flask import Flask, render_template
import requests
import json
import os
import apicontroller

app = Flask(__name__)


PORT = int(os.environ.get('PORT', 4567))

@app.route("/")
@app.route("/index")
def index():
  lastobjs = apicontroller.get_lastobjs()
  obj1 = lastobjs["objects"][1]
  obj2 = lastobjs["objects"][2]
  obj3 = lastobjs["objects"][3]
  obj4 = lastobjs["objects"][4]
  obj5 = lastobjs["objects"][5]
  obj6 = lastobjs["objects"][6]
  return render_template('index.html', name1=obj1["name"], location1=obj1["location"], cctv1=obj1["cctv"], a1=obj1["abandoned"], e1=obj1["elevator"], year1=obj1["yearbuilt"], name2=obj2["name"], location2=obj2["location"], cctv2=obj2["cctv"], a2=obj2["abandoned"], e2=obj2["elevator"], year2=obj2["yearbuilt"], name3=obj3["name"], location3=obj3["location"], cctv3=obj3["cctv"], a3=obj3["abandoned"], e3=obj3["elevator"], year3=obj3["yearbuilt"], name4=obj4["name"], location4=obj4["location"], cctv4=obj4["cctv"], a4=obj4["abandoned"], e4=obj4["elevator"], year4=obj4["yearbuilt"], name5=obj5["name"], location5=obj5["location"], cctv5=obj5["cctv"], a5=obj5["abandoned"], e5=obj5["elevator"], year5=obj5["yearbuilt"], name6=obj6["name"], location6=obj6["location"], cctv6=obj6["cctv"], a6=obj6["abandoned"], e6=obj6["elevator"], year6=obj6["yearbuilt"]) 

@app.route("/about")
def about():
  return render_template('about.html')

@app.route("/map")
def map():
  return render_template('osmb-map.htm')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=PORT, debug=True)
