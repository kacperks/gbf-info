#GBF-INFO rest API
#Based on HackChat's API by Lubek from CubeSoftware
#Created by CubeSoftware
#-------------------------------
#Copyright (c) 2021-2022 CubeSoftware
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the #Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the #Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A #PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION #OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import datetime
import os
import uuid
import configparser
from functools import wraps
import jwt # PYJWT
from flask import Flask, request, jsonify, make_response
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
global rankid

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SECRET_KEY'] = 'This is secret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
secret_key = app.config['SECRET_KEY']
db = SQLAlchemy(app)
ma = Marshmallow(app)

class Building(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    public_id = db.Column(db.Integer)
    name = db.Column(db.String(50))
    location = db.Column(db.String(100))
    elevator = db.Column(db.String(50))
    cctv = db.Column(db.Boolean())
    abandoned = db.Column(db.Boolean())
    yearbuilt = db.Column(db.Integer)


class BuildingSchema(ma.Schema):
    class Meta:
        fields = ('id', 'public_id', 'name', 'location', 'elevator', 'cctv', 'abandoned', 'yearbuilt')

_BuildingSchema = BuildingSchema()
_BuildingsSchema = BuildingSchema(many=True)

@app.route('/add_building', methods=['POST'])
def add_building():
    if request.method == 'POST':
        try:
            public_id = str(uuid.uuid4())
            name = request.json['name']
            location = request.json['location']
            elevator = request.json['elevator']
            cctv = request.json['cctv']
            abandoned = request.json['abandoned']
            yearbuilt = request.json['yearbuilt']
            
            # check if building already exists
            building = Building.query.filter_by(location=location).first()
            if not building:
                new_building = Building(public_id=public_id, name=name, location=location, elevator=elevator, cctv=cctv, abandoned=abandoned, yearbuilt=yearbuilt)
                db.session.add(new_building)
                db.session.commit()
                return jsonify({'message': 'New Building created successfully'})
            else:
                return jsonify({'message': 'Building already exists at the index'})
        except Exception as e:
            return jsonify({'message': str(e)})

@app.route('/lastobjs', methods=['GET'])
def get_lastobjs():
    lastobjs = Building.query.order_by(Building.id.desc()).limit(6).all()
    return jsonify({'objects': _BuildingsSchema.dump(lastobjs), 'code': 200})
# Run the app
if __name__ == '__main__':
    app.run(debug=True)
